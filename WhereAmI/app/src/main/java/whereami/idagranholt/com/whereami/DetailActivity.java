package whereami.idagranholt.com.whereami;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.List;
import java.text.DecimalFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import static java.lang.String.format;

/*
* Detail Activity is started from mainActivity when Detail button is clicked
* It gets location object from the intent and shows information
* about the users current location.
* It shows town name, latitude, longitude, altitude and current temperature.
* It also saves The City name and current location in a SqlLite datbase.
* */
public class DetailActivity extends Activity implements View.OnClickListener {

    private static String BASE_URL = "http://api.openweathermap.org/data/2.5/weather?mode=xml&q=";
    String town;

    /*
    * Gets Text Views and hides Views until data is retrieved.
    * Gets location object from intent.
    * Gets latitude, longitude and altitude from location object.
    * Tries to get city name from reverse geocoding.
    * Formats Title with city name
    * Formats latitude and longitude string.
    * Shows view and formats string if altitude was found
    * Gets weather data from openweathermap.com
    *
    * Formatiing strings
    * http://developer.android.com/guide/topics/resources/string-resource.html
    * http://developer.android.com/reference/java/util/Formatter.html
    *
    * Current wather data:
    * Looked at the code form git repository File Download Demo and:
    * http://openweathermap.org/current
    * For UTF-8
    * http://stackoverflow.com/questions/17901133/android-post-utf-8-httpurlconnection
    * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        TextView detailTitleView = (TextView) findViewById(R.id.detail_title_textview);
        TextView detailLatLngView = (TextView) findViewById(R.id.detail_latlng_textview);
        TextView detailInfoView = (TextView) findViewById(R.id.detail_info_textview);

        Button historyButton = (Button) findViewById(R.id.history_button);
        historyButton.setOnClickListener(DetailActivity.this);
        detailInfoView.setVisibility(View.GONE);

        Location location = getIntent().getExtras().getParcelable("LOCATION");
        double lat =  (location.getLatitude());
        double lng = (location.getLongitude());
        int msl = (int)location.getAltitude();

        town = getTown(lat, lng);

        Resources res = getResources();

        detailTitleView.setTypeface(null, Typeface.BOLD);
        String titleText = format(res.getString(R.string.detail_title), town);
        detailTitleView.setText(titleText);

        String latLngText = format(res.getString(R.string.detail_latlng), lat , lng);
        detailLatLngView.setText(latLngText);

        if (msl != 0){
            detailInfoView.setVisibility(View.VISIBLE);
            String infoText = format(res.getString(R.string.detail_info), town , msl);
            detailInfoView.setText(infoText);
        }

        String uniTown = "";
        try {
             uniTown = URLEncoder.encode(town, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            Log.d("URL:   ", BASE_URL + uniTown);
            new GetWeather().execute(new URL(BASE_URL+ uniTown));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    * Starts the History Activity
    * */
    @Override
    public void onClick(View view) {
        Intent intent = new Intent(DetailActivity.this, HistoryActivity.class);
        startActivity(intent);
    }
    /*
    * Tries to get city name from reverse geocoding
    *
    * */
    private String getTown( double lat, double lng) {
        String town = "Unknown";
        try {
            List<Address> geocodeMatches  = new Geocoder(DetailActivity.this)
                    .getFromLocation(lat, lng, 1);
            if (!geocodeMatches.isEmpty()) {
                town = geocodeMatches.get(0).getLocality();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return town;
    }

    /*
    * Task, responsible for downloading a XML from a a given URL.
    * This task will execute the download asynchronously.
    * When the async task is ready  it formats the temperature view
    * and saves city name and current temperature in the location database
    *
    * Looked at git repository for File Download Demo and
    * SQLLite notes demo for inspiration
    * */
    private class GetWeather extends AsyncTask<URL, Void, String>{

        @Override
        protected String doInBackground(URL... urls) {
            assert urls.length == 1;
            return downloadWeather(urls[0]);
        }
        @Override
        protected void onPostExecute(String results) {
            double kelvin = Double.parseDouble(results);
            double celsius = kelvin - 273.15;
            DecimalFormat dF = new DecimalFormat("#.#");
            String temperatureInCelsius = "The local temperature is "
            + String.valueOf(dF.format(celsius)) + " degrees Celsius";
            TextView weatherTextView = (TextView) findViewById(R.id.detail_weather);
            weatherTextView.setText(temperatureInCelsius);

            final Loc loc = new Loc(town, String.valueOf(dF.format(celsius)));
            final DatabaseHelper dbHelper = new DatabaseHelper(DetailActivity.this);
            loc.save(dbHelper);
        }
    }

    /**
     * Downloads and parses an xml with current weather data
     * from a city name passed in the URL and then
     * Gets the "temperature" tag in the XML,
     * and then gets the value of the "value" attribute in "temperature"
     * Returns the current temperature as a string.
     *
     * Looked at git repository for File Download Demo for inspiration
     */
    private String downloadWeather(final URL url) {
        InputStream in;
        String tempString = "";

        try {
            in = openHttpConnection(url);
            Document doc = null;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db;

            try {
                db = dbf.newDocumentBuilder();
                doc = db.parse(in);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }

            doc.getDocumentElement().normalize();
            NodeList itemNodes = doc.getElementsByTagName("temperature");
            Node itemNode = itemNodes.item(0);
            if (itemNode.getNodeType() == Node.ELEMENT_NODE) {
                Element itemElement = (Element) itemNode;
                tempString = itemElement.getAttribute("value");
             }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return tempString;
    }


    /**
     * Handles some of the complexity of opening up a HTTP connection
     *
     * Code is reused from git repository for File Download Demo.
     */
    private InputStream openHttpConnection(final URL url) throws IOException {
        InputStream in = null;
        int response = -1;
        final URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP connection");
        }

        try {
            final HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return in;
    }


}
