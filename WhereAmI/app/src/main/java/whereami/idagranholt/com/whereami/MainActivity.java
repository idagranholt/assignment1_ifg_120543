package whereami.idagranholt.com.whereami;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

/*
* Main Activity launches when the app is startet.
* On start it show a button and a map
* When user presses the button current location is shown on map
* and a Detail button appears.
* The Detail button sends the use to the Detail Activity.
*
* For LocationListener
* http://developer.android.com/reference/android/location/LocationListener.html
* */
public class MainActivity extends Activity implements LocationListener {

    static final String LOCATION_FOUND = "location_found";  // For saving state.

    private LocationManager locationManager;                // For Location Manager:
    private String provider;
    private Location location;
    private GoogleMap mMap;                                 // For Google Map.
    Button detailButton;

    /*
    * Changes map height if screen is horizontal.
    * Sets listener to both Buttons.
    * Gets LocationManager and the best provider.
    * Sets up map if not been set up already
    * Hides Detail Button until Where Button is clicked.
    * Gets data from Saved Instance State.
    * Shows Detail Button, gets location and places marker
    * if Where Button is saved as clicked
    *
    * For set onClickListener:
    * http://www.raywenderlich.com/56109/make-first-android-app-part-2
    * Using two buttons:
    * http://blog.idleworx.com/2011/06/build-simple-android-app-2-button.html
    * Hide and show button:
    * http://stackoverflow.com/questions/6173400/how-to-programmatically-hide-a-button-in-android-sdk
    * Location Manager:
    * http://www.vogella.com/tutorials/AndroidLocationAPI/article.html#tutlocationapi
    * Saving state:
    * http://developer.android.com/training/basics/activity-lifecycle/recreating.html
    * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkOriantation();

        Button whereButton = (Button) findViewById(R.id.where_button);
        whereButton.setOnClickListener(where);
        detailButton = (Button) findViewById(R.id.details_button);
        detailButton.setOnClickListener(detail);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        setUpMapIfNeeded();

        detailButton.setVisibility(View.GONE);

        if (savedInstanceState != null) {
            int locationFound = savedInstanceState.getInt(LOCATION_FOUND);
            if(locationFound == 1) {
                detailButton.setVisibility(View.VISIBLE);
                getLocationAndPlaceMarker();
            }

        }

    }

    /*
    * Registers for location updates.
    * Sets up map if not been set up already.
    *
    * http://www.vogella.com/tutorials/AndroidLocationAPI/article.html#tutlocationapi
    * */
    @Override
    protected void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(provider, 1000, 10, this);
        setUpMapIfNeeded();
    }

    /*
    * Saves information on weather the Where Button have been clicked or not
    * to see if user has gotten a location or not.
    * Also saves view hierarchy states.
    *
    * http://developer.android.com/training/basics/activity-lifecycle/recreating.html
    * */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        int locationFound = 0;
        if(location != null) {
            locationFound = 1;
        }
       savedInstanceState.putInt(LOCATION_FOUND, locationFound);
       super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    *  Changes map height if screen is horizontal.
    *
    *  http://stackoverflow.com/questions/21469345/how-do-you-programmatically-change-the-width-height-of-google-maps-v2-support
    *  http://developer.android.com/reference/android/view/Display.html#getRotation()¨
    * */
    private void checkOriantation() {
        WindowManager mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = mWindowManager.getDefaultDisplay();
        if(display.getRotation() == Surface.ROTATION_90) {
            Fragment mMapFragment =  getFragmentManager()
                    .findFragmentById(R.id.map);
            ViewGroup.LayoutParams params = mMapFragment.getView().getLayoutParams();
            params.height = 300;
            mMapFragment.getView().setLayoutParams(params);
        }
    }

    /*
    * Anonymous function for onclick listener for the Where button.
    * Gets location and makes a marker on the map.
    * */
    private View.OnClickListener where = new View.OnClickListener()  {
        @Override
        public void onClick(View view){

            getLocationAndPlaceMarker();
        }
    };

    /* Anonymous function for onclick listener for the Detail Button.
    * If a location has been found sends the location object trough intent and
    * starts the Detail Activity.
    *
    * http://stackoverflow.com/questions/6839240/passing-location-data-to-another-activity-in-android
    * http://stackoverflow.com/questions/11214829/how-to-send-android-location-address-with-intent-as-putextra
    * */
    private View.OnClickListener detail = new View.OnClickListener() {
        @Override
        public void onClick(View view){
            if(location != null) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("LOCATION", location);
                startActivity(intent);
            } else {
                Toast.makeText(MainActivity.this, "You have to know where you are before getting details",
                        Toast.LENGTH_SHORT).show();
            }
        }
    };

    /*
    * Gets new location on change and makes marker on the map.
    * */
    @Override
    public void onLocationChanged(Location location) {
        int lat = (int) (location.getLatitude());
        int lng = (int) (location.getLongitude());
        String locationText = getLocationText(lat, lng);
        makeMarker(lat, lng, locationText);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Toast.makeText(MainActivity.this, "Provider has changed to:  " + provider,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }

    /*
    * If map is not already instantiated, instantiates map
    *
    * https://developers.google.com/maps/documentation/android/map
    **/
    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        }
    }

    /*
    * Gets last known location from the best provider available.
    * If a location was found,
    * gets latitude and longitude from location.
    * Gets Location text and sets a marker on the map.
    * Makes Detail Button visible
    *
    * http://www.vogella.com/tutorials/AndroidLocationAPI/article.html#tutlocationapi
    * */
    private void getLocationAndPlaceMarker() {
        location = locationManager.getLastKnownLocation(provider);
        if (location != null) {
            double lat =  (location.getLatitude());
            double lng = (location.getLongitude());
            String locationText = getLocationText(lat, lng);
            makeMarker(lat, lng, locationText);
            detailButton.setVisibility(View.VISIBLE);

        } else {
            Toast.makeText(MainActivity.this, "Cannot find your position ",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /*
    * Tries to get address data from reverse geocoding
    * If an address was found saves it for marker.
    *
    * http://developer.android.com/training/location/display-address.html
    * http://developer.android.com/reference/android/location/Address.html
    * */
    private String getLocationText( double lat, double lng) {
        String lText = "Your location";
        try {
            List<Address> geocodeMatches  = new Geocoder(MainActivity.this)
                    .getFromLocation(lat, lng, 1);
            if (!geocodeMatches.isEmpty()) {
                lText = geocodeMatches.get(0).getAddressLine(0) + "\n"
                        + geocodeMatches.get(0).getAddressLine(1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lText;
    }

    /*
    * Makes marker in map for current position.
    * Zooms in to current position.
    *
    * http://www.androidhive.info/2013/08/android-working-with-google-maps-v2/
    * */
    private void makeMarker(double lat, double lng, String locationText) {
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(lat, lng)).title(locationText);
        mMap.addMarker(marker);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                new LatLng(lat, lng)).zoom(12).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

}
