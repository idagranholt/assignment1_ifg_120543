package whereami.idagranholt.com.whereami;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/*
* Shows a list of log history from the SqlLite database.
* */
public class HistoryActivity extends Activity {

    ListView listView;
    ArrayAdapter mArrayAdapter;
    ArrayList mHistoryList = new ArrayList();

    /*
    * Calls on the getAll function on Loc class
    * to get all rows from the "location" table in the SqlLite database
    * Puts the result in the ListView
    *
    * Looked at git repositry SqlLite notes demo for inspiration and:
    * http://www.raywenderlich.com/56109/make-first-android-app-part-2
    * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        listView = (ListView) findViewById(R.id.list_view);
        mArrayAdapter = new ArrayAdapter<Loc>(this, android.R.layout.simple_list_item_1,
                Loc.getAll(new DatabaseHelper(this)));
        listView.setAdapter(mArrayAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
