package whereami.idagranholt.com.whereami;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.List;

/**
 *A class which represents a single Location
 *
 * Reused code from git repository SQLLite notes demo,
 * and made changes to fit this application
 */
public class Loc {



    long id;
    String town;
    String degrees;

    private Loc() {}

    public Loc(final String town, final String degrees) {
        this.town = town;
        this.degrees = degrees;
    }
    /*
    * Saves current location and current temperature in the "location" table.
    * */
    public void save(DatabaseHelper dbHelper) {
        final ContentValues values = new ContentValues();
        values.put(TOWN, this.town);
        values.put(DEGREES, this.degrees);

        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        this.id = db.insert(LOC_TABLE_NAME, null, values);
        db.close();
    }
    /*
    * Goes trough the table "location". Begins with the last entry
    * */
    public static Loc[] getAll(final DatabaseHelper dbHelper) {
        final List<Loc> locs = new ArrayList<Loc>();
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final Cursor c = db.query(LOC_TABLE_NAME,
                new String[] { ID, TOWN, DEGREES}, null, null, null, null, null);
        c.moveToLast();
        while(!c.isBeforeFirst()) {
            final Loc loc = cursorToLoc(c);
            locs.add(loc);
            c.moveToPrevious();
        }
        c.close();
        return locs.toArray(new Loc[locs.size()]);
    }

    public static Loc cursorToLoc(Cursor c) {
        final Loc loc = new Loc();
        loc.setTown(c.getString(c.getColumnIndex(TOWN)));
        loc.setDegrees(c.getString(c.getColumnIndex(DEGREES)));
        return loc;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public void setDegrees(String degrees) {
        this.degrees = degrees;
    }

    /*
    * Formats string for the ListView items
    * */
    public String toString() {
        return "Town: " + this.town + " \nDegrees: "+ this.degrees + " Celsius";
    }


    // Table name
    public static final String LOC_TABLE_NAME = "location";
    // column names
    static final String ID = "id"; //
    static final String TOWN = "town";
    static final String DEGREES = "degrees";
    // SQL statement to create our table
    public static final String LOC_CREATE_TABLE = "CREATE TABLE " + Loc.LOC_TABLE_NAME + " ("
            + Loc.ID + " INTEGER PRIMARY KEY,"
            + Loc.TOWN + " TEXT,"
            + Loc.DEGREES + " TEXT"
            + ");";

}
